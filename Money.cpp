/* 
 * File:   Money.cpp
 * Author: Grupo 22
 * 
 * Created on March 11, 2012, 12:43 PM
 */

#include "Money.h"
#include <iostream>
#include <math.h>

Money::Money() {
    currency = DOLAR;
    amount = 0;
}

Money::Money(t_currency currency, double amount) {
    this->currency = currency;
    this->amount   = amount;
}

Money::Money(const Money& orig) {
    currency = orig.currency;
    amount   = orig.amount;
}

// Definition and initialization of static members
double Money::quote = 19.35;

//
t_currency Money::getCurrency() const {
    return currency;
}

double Money::getAmount() const {
    return amount; 
}

Money Money::operator= (const Money& money) {
    currency = money.currency;
    amount   = money.amount;
    return *this;
}

Money Money::operator+ (const Money& money) const {
    return Money(currency, amount + getSameCurrencyValue(money));
}

Money Money::operator+ (const double& op) const {
    return Money(currency, amount+op);
}

Money Money::operator- (const Money& money) const {
    return Money(currency, amount - getSameCurrencyValue(money));
}

Money Money::operator- (const double& op) const {
    return Money(currency, amount-op);
}

Money Money::operator* (const double& op) const {
    return Money(currency, amount*op);
}

// Overloaded comparisson operators
bool Money::operator<  (const Money& money) const {
    return ((amount - getSameCurrencyValue(money)) < 0);
}

bool Money::operator<= (const Money& money) const {
    return ((amount - getSameCurrencyValue(money)) <= 0);
}

bool Money::operator>  (const Money& money) const {
    return ((amount - getSameCurrencyValue(money) > 0));
}

bool Money::operator>= (const Money& money) const {
    return ((amount - getSameCurrencyValue(money)) >= 0);
}

bool Money::operator== (const Money& money) const {
    return (fabs(amount - getSameCurrencyValue(money)) < 0.0001);
}

bool Money::operator!= (const Money& money) const {
    return !(operator==(money));
}

// Other function 
double Money::toDolares() const {
    return (amount/quote);
}

double Money::toPesos() const {
    return (amount*quote);
}

double Money::getSameCurrencyValue (Money money) const{
    double finalValue = money.amount;
    if (money.currency != currency){
        if (money.currency == PESO) {
                finalValue = money.toDolares();
        }
        else {
                finalValue = money.toPesos();
        }
    }
    return finalValue;
}

Money::~Money() {}

Money operator+ (const double& op, const Money& money) {
    return Money(money.getCurrency(), money.getAmount()+op);
}

Money operator- (const double& op, const Money& money) {
    return Money(money.getCurrency(), money.getAmount()-op);
}

Money operator* (const double& op, const Money& money) {
    return Money(money.getCurrency(), money.getAmount()*op);
}

double redondear(const double aRedondear) {
    double res = aRedondear*100;
    double aux;
    double* parte_entera = new double;
    aux = modf(res, parte_entera);
    if (aux >= 0.5) {
        res ++;
    }
    delete parte_entera;
    res = floor(res)/100;
    return res;
}

ostream& operator<< (ostream& out, Money& money) {
    if (money.getCurrency() == DOLAR) {
        out << "U$S" << redondear(money.getAmount());
    }
    else {
        out << "$" << redondear(money.getAmount());
    }
    return out;
}
