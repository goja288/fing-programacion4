/* 
 * File:   main.cpp
 * Author: Grupo 22
 *
 */
 
#include "Mensual.h"
#include "Jornalero.h"
#include <stdio.h>
#include <iostream>
#include <stdexcept>

using namespace std;

//Menu que prueba todas las operaciones
void menu(Mensual** array_mensuales, Jornalero** array_jornaleros)
{
    //inicializaciones + menu
    int eleccion = 0;
    while (eleccion != 99) {
        try {
            cout << "                *******************MENU*********************" << endl;
            cout << "                *1  Alta Mensual   (constructor por defecto)" << endl;
            cout << "                *2  Alta Jornalero (constructor por defecto)" << endl;
            cout << "                *3  Alta Mensual   (constructor comun)      " << endl;
            cout << "                *4  Alta Jornalero (constructor comun)      " << endl;
            cout << "                *5  Alta Mensual   (constructor por copia)  " << endl;
            cout << "                *6  Alta Jornalero (constructor por copia)  " << endl; 
            cout << "                *7  Baja Mensual                            " << endl;
            cout << "                *8  Baja Jornalero                          " << endl;
            cout << "                *9  setSalario Mensual                      " << endl;
            cout << "                *10 getSalario Mensual                      " << endl;
            cout << "                *11 setPorHora Jornalero                    " << endl;
            cout << "                *12 getPorHora Jornalero                    " << endl;
            cout << "                *13 getSalario Jornalero                    " << endl;
            cout << "                *99 SALIR!!!                                " << endl;
            cout << "                ********************************************" << endl;
            cout << "                          Ingrese un numero:                ";
            cin  >> eleccion;
            cout << "                *******************************************" << endl;

            //Despliego estado de los arrays
            cout << "Array Mensuales (vacio = -- , ocupado = o)" << endl;
            cout << "|";
            for (int i = 0; i < 20; i++) {
                cout << i << " "; 
                if (array_mensuales[i] == NULL) {
                    cout << "--";
                }
                else {
                    cout << "o ";
                }
                cout << "|";
            }
            cout << endl << endl;
            cout << "Array Jornaleros (vacio = --, ocupado = 0)" << endl;
            cout << "|";
            for (int i = 0; i < 20;i++) {
                cout << i << " ";
                if (array_jornaleros[i] == NULL) {
                    cout << "--";
                }
                else {
                    cout << "o ";
                }
                cout << "|";
            }
            cout << endl << "-----------------------------------------------------------" << endl << endl;

            switch (eleccion) {
                case 1: { // Alta Mensual (constructor por defecto)
                    int numero = 99;
                    while ((numero >= 20) or (numero < 0)) {
                        cout << "Ingrese el numero del mensual (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) {
                            if (array_mensuales[numero] == NULL) {
                                Mensual* mensual = new Mensual();
                                array_mensuales[numero] = mensual;
                            }
                            else {
                                cout << "Ya existe un mensual con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero << " no es correcto " << endl;
                        }
                    }
                break;	
                }
                case 2: { // Alta Jornalero(constructor por defecto)
                    int numero = 99;
                    while ((numero >= 20) or (numero < 0)) {
                        cout << "Ingrese el numero del jornalero (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) {
                            if (array_jornaleros[numero] == NULL) {
                                Jornalero* jornalero = new Jornalero();
                                array_jornaleros[numero] = jornalero;
                            }
                            else {
                                cout << "Ya existe un jornalero con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero << " no es correcto" << endl;
                        }
                    }
                break;	
                }
                case 3: { // Alta Mensual (constructor comun)
                    Money salario;
                    int numero = 99;
                    while ((numero >= 20) or (numero < 0)) {
                        cout << "Ingrese el numero del mensual (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) { 
                            if(array_mensuales[numero] == NULL) {
                                int moneda = 0;
                                double sueldo = 0;
                                cout << "Ingrese el salario del mensual: ";
                                cin  >> sueldo;
                                cout << endl;
                                while ((moneda != 1) and (moneda != 2)) {
                                        cout << "1) PESOS - 2) DOLARES: ";
                                        cin  >> moneda;
                                        cout << endl;
                                        if (moneda == 1) {
                                                salario = Money(PESO,sueldo);
                                        }
                                        else if (moneda == 2) {
                                                salario = Money(DOLAR,sueldo);
                                        }
                                }
                                Mensual* mensual = new Mensual(salario);
                                array_mensuales[numero] = mensual;
                            }
                            else {
                                cout << "Ya existe un mensual con ese numero..." << endl;
                            }
                        }
                        else {
                             cout << numero << " no es correcto" << endl;
                        }
                    }
                break;
                }
                case 4: {// Alta Jornalero (constructor comun)
                    Money salario;
                    int numero = 99;
                    while ((numero >= 20) or (numero < 0)) {
                        cout << "Ingrese el numero del jornalero (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) {
                            if (array_jornaleros[numero] == NULL) {
                                int moneda = 0;
                                double sueldo = 0;
                                cout << "Ingrese el salario por hora del jornalero: ";
                                cin  >> sueldo;
                                cout << endl;
                                while ((moneda != 1) and (moneda != 2)) {
                                    cout <<"1) PESOS - 2) DOLARES: ";
                                    cin  >> moneda;
                                    cout << endl;
                                    if (moneda == 1) {
                                        salario = Money(PESO,sueldo);
                                    }
                                    else if (moneda == 2) {
                                        salario = Money(DOLAR,sueldo);
                                    }
                                }
                                Jornalero* jornalero = new Jornalero(salario);
                                array_jornaleros[numero] = jornalero;
                            }
                            else {
                                cout << "Ya existe un jornalero con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero << " no es correcto" << endl;
                        }                                                                  
                    }
                break;
                }
                case 5: { //Alta Mensual (constructor por copia)
                    int numero = 99;
                    int copia = 99;
                    while (((numero >= 20) or (numero < 0)) and ((copia >= 20) or (copia < 0))) {
                        cout << "Ingrese el numero del mensual donde se alojara la copia (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        cout << "Ingrese el numero del mensual que quiere copiar (0 a 19): ";
                        cin >> copia;
                        cout << endl;
                        if (((numero < 20) and (numero > -1)) and ((copia < 20) and (copia > -1))) {
                            if ((array_mensuales[numero] == NULL) and (array_mensuales[copia] != NULL)) {
                                Mensual* mensual = new Mensual(*array_mensuales[copia]);
                                array_mensuales[numero] = mensual;
                            }
                            else {
                                cout << "Error, no existe el mensual o la copia..." << endl;
                            }
                        }
                        else {
                            cout << numero << " o "<< copia << " no son correctos " << endl;
                        }
                    }
                break;
                }
                case 6: { //Alta Jornalero (constructor por copia)
                    int numero = 99;
                    int copia = 99;
                    while (((numero >= 20) or (numero < 0)) and ((copia >= 19) or (copia < 0))) {
                        cout << "Ingrese el numero del jornalero donde se alojara la copia (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        cout << "Ingrese el numero del jornalero que quiere copiar (0 a 19): ";
                        cin  >> copia;
                        cout << endl;
                        if (((numero < 20) and (numero > -1)) and ((copia < 20) and (copia > -1))) {
                            if ((array_jornaleros[numero] == NULL) and (array_jornaleros[copia] != NULL)) {
                                Jornalero* jornalero = new Jornalero(*array_jornaleros[copia]);
                                array_jornaleros[numero] = jornalero;
                            }
                            else {
                                cout << "Error, no existe el jornalero o la copia..." << endl;
                            }
                        }
                        else {
                            cout << numero << " o "<< copia << " no son correctos" << endl;
                        }
                    }
                break;
                }
                case 7: { //Baja Mensual
                    int numero = 99;
                    while ((numero >= 20) or (numero < 0)){
                        cout << "Ingrese el numero del mensual (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) {
                            if (array_mensuales[numero] != NULL) {
                                delete(array_mensuales[numero]);
                                array_mensuales[numero] = NULL;
                            }
                            else {
                                cout << "No existe un mensual con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero <<" No es correcto" << endl;
                        }
                    }
                break;	
                }
                case 8: { // Baja Jornalero
                    int numero = 99;
                    while ((numero >= 20) or (numero < 0)) {
                        cout << "Ingrese el numero del jornalero (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)){
                            if (array_jornaleros[numero] != NULL){
                                delete(array_jornaleros[numero]);
                                array_jornaleros[numero] = NULL;
                            }
                            else {
                                cout << "No existe un Jornalero con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero << " no es correcto" << endl;
                        }
                    }
                break; 
                }
                case 9: { // setSalario mensual
                    int numero = 99;
                    Money salario;
                    int moneda = 0;
                    double sueldo = 0;
                    cout << "Ingrese el salario del mensual: ";
                    cin  >> sueldo;
                    cout << endl;
                    while ((moneda != 1) and (moneda != 2)) {
                        cout << " 1) PESOS - 2) DOLARES: ";
                        cin  >> moneda;
                        cout << endl;
                        if (moneda == 1) {
                            salario = Money(PESO,sueldo);
                        }
                        else if (moneda == 2) {
                            salario = Money(DOLAR,sueldo);
                        }
                    }
                    while ((numero >= 20) or (numero < 0)) {
                        cout << "Ingrese el numero del mensual (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) {
                            if (array_mensuales[numero] != NULL) {
                                (*array_mensuales[numero]).setSalario(salario);
                            }
                            else {
                                cout << "No existe un mensual con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero << " no es correcto" << endl;
                        }
                    }
                break;	
                }
                case 10: { // getSalario Mensual
                    int numero = 99;
                    Money salario;
                    while ((numero >= 20) or (numero < 0)) {
                        cout << "Ingrese el numero del mensual (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) {
                            if (array_mensuales[numero] != NULL) {
                                salario = Money((*array_mensuales[numero]).getSalario(0));
                                cout << "El salario del mensual(" << numero << ") es: " << salario << endl;
                            }
                            else {
                                cout << "No existe un mensual con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero << " no es correcto" << endl;
                        }
                    }
                break;	
                }
                case 11: {// setPorHora (Jornalero)
                    int numero = 99;
                    Money porHora;
                    int moneda = 0;
                    double sueldo = 0;
                    cout << "Ingrese el salario por hora del jornalero: ";
                    cin  >> sueldo;
                    cout << endl;
                    while ((moneda != 1) and (moneda != 2)) {
                        cout << "1) PESOS - 2) DOLARES: ";
                        cin  >> moneda;
                        cout << endl;
                        if (moneda == 1) {
                            porHora = Money(PESO,sueldo);
                        }
                        else if (moneda == 2) {
                            porHora = Money(DOLAR,sueldo);
                        }
                    }
                    while ((numero >= 20) or (numero < 0)) {
                        cout << "Ingrese el numero del jornalero (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) {
                            if (array_jornaleros[numero] != NULL) {
                                (*array_jornaleros[numero]).setPorHora(porHora);
                            }
                            else {
                                cout << "No existe un jornalero con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero << " no es correcto" << endl;
                        }
                    }
                break;	
                }
                case 12: { // getPorHora (Jornalero)
                    int numero = 99;
                    Money porHora;
                    while ((numero >= 20) or (numero < 0)) {
                        cout << "Ingrese el numero del jornalero (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) {
                            if (array_jornaleros[numero] != NULL) {
                                porHora = Money((*array_jornaleros[numero]).getPorHora());
                                cout << "El salario por hora del jornalero (" << numero << ") es: " << porHora << endl;
                            }
                            else {
                                cout << "No existe un jornalero con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero << " no es correcto" << endl;
                        }
                    }
                break;	
                }
                case 13: { // getSalario Jornalero
                    int numero = 99;
                    double horas = 0;
                    Money salario;
                    while ((numero >= 20) or (numero < 0)){
                        cout << "Ingrese el numero del jornalero (0 a 19): ";
                        cin  >> numero;
                        cout << endl;
                        cout << "Ingrese el numero de horas trabajadas: ";
                        cin  >> horas;
                        cout << endl;
                        if ((numero < 20) and (numero > -1)) {
                            if (array_jornaleros[numero] != NULL) {
                                salario = Money((*array_jornaleros[numero]).getSalario(horas));
                                cout << "El salario del jornalero (" << numero << ") por " << horas << " horas es:" << salario << endl;
                            }
                            else {
                                cout << "No existe un jornalero con ese numero..." << endl;
                            }
                        }
                        else {
                            cout << numero << " no es correcto" << endl;
                        }
                    }
                break;	
                }
                case 99: { // Salir
                break;
                }
                default: {
                    cout <<"Opcion incorrecta" << endl;
                }			
            } // switch
        }
        catch (std::invalid_argument& e)
        {
            cout << e.what() << endl;
        }
    } // while de eleccion

} // menu


int main ()
{
    Mensual* array_mensuales[20];
    Jornalero* array_jornaleros[20];
    //Creo e inicializo los objetos
    for (int i = 0; i < 20; i++){
        array_mensuales[i] = NULL;
        array_jornaleros[i] = NULL;
    }

    //main loop...
    menu(array_mensuales,array_jornaleros); 
    
    //Destruyo estructuras
    for (int i = 0; i < 20; i++){
        if (array_mensuales[i] != NULL) {
            delete (array_mensuales[i]);
        }
        if (array_jornaleros[i] != NULL) {
            delete (array_jornaleros[i]);
	}
    }
        
    return 0;

}

