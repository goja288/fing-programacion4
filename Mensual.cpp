/* 
 * File:   Mensual.cpp
 * Author: Grupo 22
 * 
 * Created on March 14, 2012, 4:35 PM
 */

#include "Mensual.h"
#include <stdexcept>

// constructors
Mensual::Mensual() {
    salario = Money();
}

Mensual::Mensual(const Mensual& orig) {
    salario = orig.salario;
}

Mensual::Mensual(Money& pay) {
    salario = pay;
}

// inspectors
Money Mensual::getSalario(double hours) {
    if (hours < 0) {
        throw std::invalid_argument("Argumento invalido, la cantidad de horas no puede ser negativa\n");
    }
    else {
        return salario;
    }
}

// mutators
void Mensual::setSalario(Money& money) {
    salario = money;
}

//destructor
Mensual::~Mensual() {
    salario.~Money();
}

