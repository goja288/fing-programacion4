/* 
 * File:   main-part1.cpp
 * Author: Grupo 22
 *
 * Created on March 11, 2012, 1:26 PM
 */

#include <cstdlib>
#include <stdio.h>
#include <iostream>
#include <stdexcept>
#include <string.h>
#include "Money.h"

using namespace std;

bool testMode;

void 
ReadOption(int &option) {
    cin >> option;
    if (option < 1 || option > 4) {
        throw std::invalid_argument("Opcion no permitida, ingrese 1, 2, 3, o 4");
    }
}

void
ChooseOption(int &option) {
    if (!testMode) {
        cout << endl;
        cout << "1-Crear"  << endl;
        cout << "2-Listar" << endl;
        cout << "3-Operar" << endl;
        cout << "4-Salir"  << endl;
        cout << endl;
    }
    bool invalidOption = true;
    while(invalidOption) {
        invalidOption = false;
        try {
                ReadOption(option);
        }
        catch(std::invalid_argument& e) {
            invalidOption = true;
            cout << e.what( ) << '\n';
        }
    }
}

void 
ReadFormat(int &format) {
    cin >> format;
    if (format < 1 || format > 3) {
        throw std::invalid_argument("Opcion no permitida, ingrese 1, 2 o 3");
    }    
}

void
ChooseFormat(int &format) {
    if(!testMode) {
        cout << "1- [money]  [operator] [money]" << endl;
        cout << "2- [number] [operator] [money]" << endl;
        cout << "3- [money]  [operator] [number]" << endl;
        cout << "Ingrese el numero del formato deseado: ";
    }   
    bool invalidOption = true;
    while(invalidOption) {
        invalidOption = false;
        try {
            ReadFormat(format);
        }
        catch(std::invalid_argument& e) {
            invalidOption = true;
            cout << e.what( ) << endl;
        }  
    }
}

void
ReadCurrency(t_currency &currency) {
    int index;
    if (!testMode) {
        cout << "Ingrese el numero de la moneda, 1) dolar - 2) peso: ";
    }
    cin >> index;
    if(index == 1) {
        currency = DOLAR;
    }
    else if(index == 2) {
        currency = PESO;
    }
    else {
        throw std::invalid_argument("Tipo de moneda no reconocido.");
    }
}

t_currency
ChooseCurrency() {
    bool invalidOption = true;
    t_currency currency;
    while(invalidOption) {
        invalidOption = false;
        try {
            ReadCurrency(currency);
        }
        catch(std::invalid_argument& e) {
            invalidOption = true;
            cout << e.what( ) << endl;
        }  
    }
    return currency;
}

double
ChooseAmount() {
    double amount;
    if (!testMode) {
        cout << "Ingrese el monto de la moneda: ";
    }
    cin >> amount;
    return amount;
}

void
CreateMoney(Money* &list, int& index) {
    t_currency currency = ChooseCurrency();
    double     amount   = ChooseAmount();
    list[index] = Money(currency, amount);
    index++;
}

void
ReadMoney(Money* list, int index, Money &money, int& elegido) {
    int chosenIndex;
   cin >> chosenIndex;
    if (chosenIndex < 0 || chosenIndex >= index) {
        throw std::invalid_argument("Indice no valido");
    }
    else {
        money   = list[chosenIndex];
        elegido = chosenIndex;
    }
}

void
ChooseMoney(Money* list, int index, Money &money, int& elegido) {
    if (!testMode) {
        cout << "Ingrese el indice de una moneda: ";
    }
    bool invalidOption = true;
    while(invalidOption) {
        invalidOption = false;
        try {
            ReadMoney(list, index, money, elegido);
        }
        catch(std::invalid_argument& e) {
            invalidOption = true;
            cout << e.what( ) << endl;
        }  
    }
}

void
ReadOperator(char* &oper) {
    cin >> oper;
    if (!(strcmp(oper,"=")  == 0  || strcmp(oper,"+")  == 0 ||
          strcmp(oper,"-")  == 0  || strcmp(oper,"*")  == 0 ||
          strcmp(oper,"<")  == 0  || strcmp(oper,"<=") == 0 ||
          strcmp(oper,">")  == 0  || strcmp(oper,">=") == 0 ||
          strcmp(oper,"==") == 0  || strcmp(oper,"!=") == 0)) {
        throw std::invalid_argument("Ingrese un operador válido (=, +, -, *, <, <=, >, >=, ==, !=).");
    }
}

void
ChooseOperator(char* &oper) {
    if (!testMode) {
        cout << "Ingrese el operador: ";
    }
    bool invalidOption = true;
    while(invalidOption) {
        invalidOption = false;
        try {
            ReadOperator(oper);
        }
        catch(std::exception &e) {
            invalidOption = true;
            cout << e.what( ) << endl;
        }
    }
}

void
ChooseNumber(double &number) {
    if (!testMode) {
        cout << "Ingrese el numero: ";
    }
    cin >> number;
}

void
PrintBool(Money m1, Money m2, char* oper, bool value) {
    cout << endl << "OPERACION: " << m1 << " " << oper << " " << m2 << " = ";
    if(value) {
        cout << "true" << endl;
    }
    else {
        cout << "false" << endl;
    }
}

void
Execute(Money& m1, char* oper, Money m2) {
    Money mResult;
    if (strcmp(oper, "+") == 0) {
        mResult = m1 + m2;
    }
    if (strcmp(oper, "-") == 0) {
        mResult = m1 - m2;        
    }
    if((strcmp(oper, "-") == 0) || (strcmp(oper, "+") == 0)) {
        cout << endl << "OPERACION: " << m1 << " " << oper << " " << m2 << " = " << mResult << endl;
        mResult.~Money();
    }
    
    if (strcmp(oper, "*") == 0) {
        cout << "No se aplica el operador a 2 monedas." << endl;
    }
    if (strcmp(oper, "=") == 0) {
        cout << endl << "Antes: " << m1 << "   Despues: ";
        m1 = m2;
        cout << m1 << endl;
    }
    if (strcmp(oper, "==") == 0) {
        PrintBool(m1, m2, oper, m1 == m2);
    }
    if (strcmp(oper, ">") == 0) {
        PrintBool(m1, m2, oper, m1 > m2);
    }
    if (strcmp(oper, "<") == 0) {
        PrintBool(m1, m2, oper, m1 < m2);
    }
    if (strcmp(oper, ">=") == 0) {
        PrintBool(m1, m2, oper, m1 >= m2);
    }
    if (strcmp(oper, "<=") == 0) {
        PrintBool(m1, m2, oper, m1 <= m2);
    }
    if (strcmp(oper, "!=") == 0) {
        PrintBool(m1, m2, oper, m1 != m2);
    }
}

void
Execute(Money m1, char* oper, double number) {
    Money mResult;
    if (strcmp(oper, "+") == 0) {
        mResult = m1 + number;
    }
    if (strcmp(oper, "-") == 0) {
        mResult = m1 - number;
    }
    if (strcmp(oper, "*") == 0) {
        mResult = m1 * number;
    }
    if ((strcmp(oper, "=") == 0) || (strcmp(oper, "==") == 0) || (strcmp(oper, ">") == 0) || (strcmp(oper, "!=") == 0)
            || (strcmp(oper, "<") == 0) || (strcmp(oper, ">=") == 0) || (strcmp(oper, "<=") == 0)) {
        cout << "No se aplica el " << oper << " a numeros." << endl;
    }
    else {
        cout << endl << "OPERACION: " << m1 << " " << oper << " " << number << " = " << mResult << endl;
        mResult.~Money();
    }
}

void
Execute(double number, char* oper, Money m1) {
    Money mResult;
    if (strcmp(oper, "+") == 0) {
        mResult = number + m1;
    }
    if (strcmp(oper, "-") == 0) {
        mResult = number - m1;
    }
    if (strcmp(oper, "*") == 0) {
        mResult = number * m1;
    }
    if ((strcmp(oper, "=") == 0) || (strcmp(oper, "==") == 0) || (strcmp(oper, ">") == 0) || (strcmp(oper, "!=") == 0)
            || (strcmp(oper, "<") == 0) || (strcmp(oper, ">=") == 0) || (strcmp(oper, "<=") == 0)) {
        cout << "No se aplica el " << oper << " a numeros." << endl;
    }
    else {
        cout << endl << "OPERACION: " << number << " " << oper << " " << m1 << " = " << mResult << endl;
        mResult.~Money();
    }
}

void
Operate(Money* list, int index) {
    Money m1, m2;
    char* oper = new char[3];
    int format;
    double number = 0;
    int elegido   = 0;
    int elegido1  = 0;   
    ChooseFormat(format);
    switch(format) {
        case 1:
            ChooseMoney(list, index, m1, elegido);
            ChooseOperator(oper);
            ChooseMoney(list, index, m2, elegido1);
            Execute(m1, oper, m2);
            if ((strcmp(oper,"=")  == 0)) {
                cout << "Primer indice: " << elegido << endl << "Segundo indice: " << elegido1 << endl;
                list[elegido] = m1;
            }            
            break;
        case 2:
            ChooseNumber(number);
            ChooseOperator(oper);
            ChooseMoney(list, index, m1, elegido);
            Execute(number, oper, m1);
            break;
        case 3:
            ChooseMoney(list, index, m1, elegido);
            ChooseOperator(oper);
            ChooseNumber(number);       
            Execute(m1, oper, number);
            break;
    }
    delete[] oper;
}

int main(int argc, char** argv) {
    testMode           = false;
    if (argc > 1) {
        if (strcmp(argv[1], "testMode") == 0) {
            testMode = true;
        }    
    }    
    bool salir         = false;
    Money* list        = new Money[50]; 
    int  index         = 0;
    int option;
    cout << endl << "Advertencia! es posible ingresar monedas con montos de mas de 2 cifras significativas." << endl;
    cout << "Las cuales seran tomadas en cuenta para operar pero no al imprimir." << endl << endl;
    while (!salir) {
        ChooseOption(option);
        switch (option) {
            case 1:
                CreateMoney(list, index);
                break;
            case 2:
                for(int i=0; i < index; i++) {
                    cout << i << "- " << list[i] << endl;
                }
                break;
            case 3:
                Operate(list, index);
                break;
            case 4:
                for(int i = 0; i < index; i++) {
                    list[i].~Money();
                }
                delete[] list;
                salir = true;
        }        
    }
    return 0;
}
