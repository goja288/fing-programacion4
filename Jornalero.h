/* 
 * File:   Jornalero.h
 * Author: Grupo 22
 *
 * Created on March 14, 2012, 4:36 PM
 */

#ifndef JORNALERO_H
#define	JORNALERO_H

#include "Empleado.h"

class Jornalero : public Empleado {
public:
    // constructors 
    Jornalero();
    Jornalero(const Jornalero& orig);
    Jornalero(Money& money);
    
    // destructor
    virtual ~Jornalero();
    
    // inspectors
    Money getSalario(double);
    Money getPorHora();
    
    // mutators
    void  setPorHora(Money&);
    
private:
    Money porHora;
    
};

#endif	/* JORNALERO_H */
