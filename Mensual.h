/* 
 * File:   Mensual.h
 * Author: Grupo 22
 *
 * Created on March 14, 2012, 4:35 PM
 */

#ifndef MENSUAL_H
#define	MENSUAL_H

#include "Empleado.h"

class Mensual : public Empleado {
public:
    // constructors
    Mensual();
    Mensual(const Mensual& orig);
    Mensual(Money& pay);
    
    // destructor
    virtual ~Mensual();
    
    // inspectors
    Money getSalario(double);
    
    // mutators
    void  setSalario(Money&);
    
private:
    Money salario;

};

#endif	/* MENSUAL_H */
