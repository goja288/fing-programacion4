/* 
 * File:   Money.h
 * Author: Grupo 22
 *
 * Created on March 11, 2012, 12:43 PM
 */

#ifndef MONEY_H
#define	MONEY_H

#include <iostream>

using namespace std;

enum t_currency { DOLAR, PESO };

class Money {
public:

    Money(); // default constructor 
    Money(t_currency currency, double amount); // constructor
    Money(const Money& orig); // copy constructor
    virtual ~Money(); // destructor
    
    // Accessor functions
    t_currency  getCurrency() const;
    double      getAmount() const;

    // Overloaded assignation operator
    virtual Money operator= (const Money& money); // asignación
    
    // Overloaded arithmetic operators
    virtual Money operator+ (const Money& money) const; // suma entre Money y Money
    virtual Money operator+ (const double& op) const;   // suma entre Money y double
    virtual Money operator- (const Money& money) const; // resta entre Money y Money
    virtual Money operator- (const double& op) const;   // resta entre Money y double
    virtual Money operator* (const double& op) const;   // producto entre Money y double

    // Overloaded comparisson operators
    virtual bool operator<  (const Money& money) const;
    virtual bool operator<= (const Money& money) const;
    virtual bool operator>  (const Money& money) const;
    virtual bool operator>= (const Money& money) const;
    virtual bool operator== (const Money& money) const;
    virtual bool operator!= (const Money& money) const;
    
    double getSameCurrencyValue (Money money) const;
    
    // Other function 
    double toDolares() const;
    double toPesos() const;
  
private:
    // Data members
    t_currency currency;  
    double amount;
    
    // Static values
    static double quote; 

};

Money operator+ (const double& op, const Money& money); // suma entre double y Money
Money operator- (const double& op, const Money& money); // resta entre double y Money
Money operator* (const double& op, const Money& money); // producto entre double y Money

double redondear(const double aRedondear);

ostream& operator<< (ostream& out, Money& money);
 

#endif	/* MONEY_H */
