/* 
 * File:   Empleado.h
 * Author: Grupo 22
 *
 * Created on March 14, 2012, 4:30 PM
 */

#ifndef EMPLEADO_H
#define	EMPLEADO_H

#include "Money.h"

class Empleado {
public:
    // constructor
    Empleado();
    
    // destructor
    virtual ~Empleado();
    
    // inspectors 
    virtual Money getSalario(double) = 0;
    
};

#endif	/* EMPLEADO_H */
