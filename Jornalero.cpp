/* 
 * File:   Jornalero.cpp
 * Author: Grupo 22
 * 
 * Created on March 14, 2012, 4:36 PM
 */

#include "Jornalero.h"
#include <stdexcept>

// constructors
Jornalero::Jornalero() {
    Money money = Money();
    porHora = money;
} 

Jornalero::Jornalero(const Jornalero& orig) {
    porHora = orig.porHora;
}

Jornalero::Jornalero(Money& money) {
    porHora = money;
} 

// inspectors
Money Jornalero::getSalario(double hours) {
    if (hours < 0) {
        throw std::invalid_argument("Argumento invalido, la cantidad de horas no puede ser negativa\n");
    }
    else {
        return (porHora)*hours;
    }
} 

Money Jornalero::getPorHora() {
    return porHora;
}

void Jornalero::setPorHora(Money& ph) {
    porHora = ph;
}

// destructor
Jornalero::~Jornalero() {
    porHora.~Money(); 
}
